from django.urls import reverse
from django.shortcuts import redirect
from django.views.generic import TemplateView

from user.views import RegistrationView


class IndexView(TemplateView):

    template_name = 'main.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            registration_view = RegistrationView()
            registration_view.request = request
            return RegistrationView.dispatch(registration_view, request)

        return redirect(reverse('profile', kwargs={'username': request.user.username}))
