from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect
from django.views.generic.edit import FormView

from . forms import UserForm


class RegistrationView(FormView):

    form_class = UserForm
    success_url = '/'
    template_name = 'registration/registration.html'

    def get_context_data(self, **kwargs):
        c = super(RegistrationView, self).get_context_data(**kwargs)
        c['login_form'] = AuthenticationForm(self.request)
        return c


    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect(self.get_success_url())
