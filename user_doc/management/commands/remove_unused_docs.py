from django.db import transaction
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from user_doc.models import Doc
from user_doc.storage import HashFileStorage


class Command(BaseCommand):
    help = 'Remove unused docs'

    @transaction.atomic
    def handle(self, *args, **options):
        fs = HashFileStorage(location=settings.MEDIA_ROOT)

        docs = Doc.objects.filter(is_used=False).select_for_update()
        for d in docs:
            if fs.exists(d.file):
                fs.open(d.file)
                fs.delete(d.file)

            d.delete()
