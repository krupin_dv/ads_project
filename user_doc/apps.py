from django.apps import AppConfig


class FileStorageConfig(AppConfig):
    name = 'user_doc'
