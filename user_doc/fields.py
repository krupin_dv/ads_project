from django.db.models.fields.files import FileField
from django.core import checks


class HashFileField(FileField):

    description = "HashFile"

    def check(self, **kwargs):
        errors = super(HashFileField, self).check(**kwargs)
        errors.extend(self._check_upload_to())
        return errors

    def _check_unique(self):
        return [] # no errors i.e. we can set unique attribute

    def _check_upload_to(self):
        if self.upload_to: # not supported yet
            return [
                checks.Error(
                    "'upload_to' is not a valid argument for a %s." % self.__class__.__name__,
                    obj=self,
                    id='fields.E200',
                )
            ]
        else:
            return []
