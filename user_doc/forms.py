from django import forms
from django.conf import settings

from . models import UserDoc


USER_DOCS_LIMIT = getattr(settings, 'USER_DOCS_LIMIT', 100)


class UserDocAddForm(forms.Form):
    file = forms.FileField()

    def __init__(self, user, *args, **kwargs):
        super(UserDocAddForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean_file(self):
        file = self.cleaned_data['file']

        if file.size == 0:
            raise forms.ValidationError("File is empty")

        if UserDoc.objects.filter(user=self.user, name=file.name).exists():
            raise forms.ValidationError("You already have a file with this name (%s)" % file.name)

        if UserDoc.objects.filter(user=self.user).count() >= USER_DOCS_LIMIT:
            raise forms.ValidationError("You have reached the limit of docs (%s)" % USER_DOCS_LIMIT)
