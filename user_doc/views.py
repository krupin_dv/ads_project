import hashlib

from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import ListView, FormView, View
from django.urls import reverse
from django.http import HttpResponse

from . models import Doc, UserDoc
from . forms import UserDocAddForm


class UserDocListView(ListView, FormView):

    profile_user = None
    form = None
    form_class = UserDocAddForm
    warning = ''
    _duplicate_warning = 'User <a href="%(profile_link)s">%(username)s</a> already have file(<a href="%(doc_link)s">%(docname)s</a>) with same content'

    def get_queryset(self):
        return UserDoc.objects.filter(user=self.profile_user).select_related('user', 'doc')

    def get(self, request, username):
        self.profile_user = get_object_or_404(User, username=username)
        return super(UserDocListView, self).get(request)

    def post(self, request, username):
        if not request.user.username == username:
            return HttpResponse('Permission denied')
        self.profile_user = request.user
        return super(UserDocListView, self).post(request)

    def get_context_data(self, **kwargs):
        if self.form:
            kwargs['form'] = self.form # preventing from new form generation

        c = super(UserDocListView, self).get_context_data(**kwargs)
        c['profile_user'] = self.profile_user
        c['warning'] = self.warning

        if self.profile_user == self.request.user:
            c['can_add'] = True
            c['can_delete'] = True

        return c

    def get_form_kwargs(self):
        kwargs = super(UserDocListView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        file = self.request.FILES['file']
        doc_name = file.name
        file.name = hashlib.sha1(file.read()).hexdigest()

        with transaction.atomic():
            # saving doc and user_doc
            try:
                doc = Doc.objects.select_for_update().get(file=file)
                if doc.is_used == False:
                    doc.is_used = True
                    doc.save()
            except Doc.DoesNotExist:
                doc = Doc(file=file, is_used=True)
                doc.save()

            user_doc = UserDoc(name=doc_name, user=user, doc=doc)
            user_doc.save()

        # add warning if another user have doc with same content
        ud = UserDoc.objects.filter(doc=doc).exclude(user=user).first()
        if ud:
            self.warning = self._duplicate_warning % {
                'profile_link': reverse('profile', kwargs={'username': ud.user.username}),
                'username': ud.user.username,
                'doc_link': ud.get_link_url(),
                'docname': ud.name,
            }

        self.form = self.form_class(user)  # empty form
        return self.get(self.request, user.username)

    def form_invalid(self, form):
        user = self.request.user
        self.form = form
        return self.get(self.request, user.username)


class UserDocDeleteView(View):

    @transaction.atomic
    def get(self, request, pk):
        ud = get_object_or_404(UserDoc.objects.filter(pk=pk, user=request.user).select_for_update())
        ud.delete()

        # if no any references on doc then mark him as non used for futher deletion
        doc = ud.doc
        if not UserDoc.objects.filter(doc=doc).exists():
            doc.is_used = False
            doc.save()

        return redirect(reverse('profile', kwargs={'username': request.user.username}))
