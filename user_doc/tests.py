import os

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase


class UserDirTestCase(TestCase):

    def test_userdir_creation_deletion(self):
        u = User(username='user1', email='user1@test.ru', password='u1234567')

        user_dir = os.path.join(settings.MEDIA_ROOT, u.username)
        self.assertFalse(os.path.lexists(user_dir))

        u.save() # creation
        self.assertTrue(os.path.isdir(user_dir))

        u.delete() # deletion
        self.assertFalse(os.path.lexists(user_dir))
