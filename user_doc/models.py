import os
import subprocess

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from . storage import HashFileStorage
from . fields import HashFileField


class Doc(models.Model):
    file = HashFileField(unique=True, storage=HashFileStorage())
    size = models.PositiveIntegerField()
    is_used = models.BooleanField()

    def save(self, *args, **kwargs):
        self.size = self.file.size
        super(Doc, self).save(*args, **kwargs)


class UserDoc(models.Model):
    user = models.ForeignKey(User)
    doc = models.ForeignKey(Doc)
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ['pk']
        unique_together = ('user', 'name')

    def get_link_url(self):
        return settings.MEDIA_URL + self.user.username + '/' + self.name

    def make_symbol_link_to_doc(self):
        doc_path = self.doc.file.path
        user_dir = os.path.join(settings.MEDIA_ROOT, self.user.username)
        userdoc_path = os.path.join(user_dir, self.name)

        subprocess.call(['ln', '-s', doc_path, userdoc_path]) # make the symbol link

    def delete_symbol_link(self):
        user_dir = os.path.join(settings.MEDIA_ROOT, self.user.username)
        userdoc_path = os.path.join(user_dir, self.name)

        subprocess.call(['rm', userdoc_path])

    def save(self, *args, **kwargs):
        super(UserDoc, self).save(*args, **kwargs)
        self.make_symbol_link_to_doc()

    def delete(self, *args, **kwargs):
        super(UserDoc, self).delete(*args, **kwargs)
        self.delete_symbol_link()

from . import signals
