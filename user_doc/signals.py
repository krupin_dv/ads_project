import os
import subprocess

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


@receiver(post_save, sender=User)
def make_userdir(sender, instance, created, **kwargs):
    if created:
        user_dir = os.path.join(settings.MEDIA_ROOT, instance.username)
        subprocess.call(['mkdir', user_dir])


@receiver(post_delete, sender=User)
def delete_userdir(sender, instance, **kwargs):
    user_dir = os.path.join(settings.MEDIA_ROOT, instance.username)
    subprocess.call(['rm', user_dir, '-R'])